
### What is this repository for? ###

* TestFLO Reports is an extension to Atlassian JIRA. It provides reports and exports for TestFLO plugin https://marketplace.atlassian.com/plugins/com.intenso.jira.plugins.suiTest/server/overview
* We give you opportunity to add extra exporting/reporting features that meet you needs. If you need more REST API capabilities just let us know at plugins@intenso.com.pl
* For current features list see https://intenso.atlassian.net/wiki/display/FLO/TestFLO+-+Reports
* Currently supported TestFLO version is 4.2.1+
* Currently supported JIRA version is 7+

### How do I participate? ###

* You can fork from master branch, 
* Implement new or modify existing feature (https://developer.atlassian.com/docs/getting-started),
* Create Pull Request,
* We (InTENSO) will verify and merge changes, then release new version on Atlassian Marketplace.
