package com.intenso.jira.plugins.tms.reports.integration.agile.model;

/**
 * @author InTENSO
 *
 */
public class TCObject {

	private Long id;
	private String key;
	private Boolean resolution;

	public TCObject(Long id, String key, Boolean resolution) {
		this.id = id;
		this.key = key;
		this.resolution = resolution;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Boolean getResolution() {
		return resolution;
	}

	public void setResolution(Boolean resolution) {
		this.resolution = resolution;
	}

}
