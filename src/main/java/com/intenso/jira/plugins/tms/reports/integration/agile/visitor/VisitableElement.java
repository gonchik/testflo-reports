package com.intenso.jira.plugins.tms.reports.integration.agile.visitor;

/**
 * @author InTENSO
 *
 */
public interface VisitableElement {

	public void accept(Visitor visitor);

}
