package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RapidView {

	@JsonProperty
	private Integer id;
	@JsonProperty
	private String name;
	@JsonProperty
	private Boolean canEdit;
	@JsonProperty
	private Boolean sprintSupportEnabled;
	@JsonProperty
	private Boolean showDaysInColumn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getCanEdit() {
		return canEdit;
	}

	public void setCanEdit(Boolean canEdit) {
		this.canEdit = canEdit;
	}

	public Boolean getSprintSupportEnabled() {
		return sprintSupportEnabled;
	}

	public void setSprintSupportEnabled(Boolean sprintSupportEnabled) {
		this.sprintSupportEnabled = sprintSupportEnabled;
	}

	public Boolean getShowDaysInColumn() {
		return showDaysInColumn;
	}

	public void setShowDaysInColumn(Boolean showDaysInColumn) {
		this.showDaysInColumn = showDaysInColumn;
	}

}
