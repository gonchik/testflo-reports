package com.intenso.jira.plugins.tms.export.rest;

import java.util.List;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.intenso.jira.plugins.tms.reports.reports.SubTasksColorStatusCFConfigBean;

/**
 * @author InTENSO
 *
 */
public interface RestCallService {

	String executeRestCall(String path);

	TmsConfigurationBean getConfiguration();
	List<String> getAdditionalTestPlanIssueTypes(Long projectId);
	CustomField getStepsCf();
	CustomField getRequirementCf();
	List<CustomField> getProgressCustomFields(Issue sampleTP);
	SubTasksColorStatusCFConfigBean getSubtasksColorStatusCfConfig(Long idAsLong, Long id, Long projectId);
	SubTasksColorStatusCFConfigBean getSubtasksColorStatusCfConfig(Long idAsLong, Long id, Issue issue);
	boolean testConnection();
}
