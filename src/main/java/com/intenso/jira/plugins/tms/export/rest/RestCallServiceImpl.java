package com.intenso.jira.plugins.tms.export.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.intenso.jira.plugins.tms.reports.reports.SubTasksColorStatusCFConfigBean;

/**
 * @author InTENSO
 *
 */
public class RestCallServiceImpl implements RestCallService {

	private static final Logger log = LoggerFactory.getLogger(RestCallServiceImpl.class);

	private final CustomFieldManager customFieldManager;

	public RestCallServiceImpl(CustomFieldManager customFieldManager) {
		super();
		this.customFieldManager = customFieldManager;
	}

	@Override
	public String executeRestCall(String path) {
		String result = null;
		try {
			URL url = new URL(getBaseUrl() + path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				log.error("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			result = sb.toString();
			conn.disconnect();

		} catch (MalformedURLException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return result;
	}

	@Override
	public TmsConfigurationBean getConfiguration() {
		String executeRestCall = executeRestCall("/rest/test-flo/1.0/configuration/global");
		TmsConfigurationBean result = null;
		if (StringUtils.isNotBlank(executeRestCall)) {
			JSONObject json;
			try {
				result = new TmsConfigurationBean();
				json = new JSONObject(executeRestCall);
				result.setExportFilterId(json.getLong("exportFilterId"));
				result.setExportTcFilterId(json.getLong("exportTcFilterId"));
				result.setExportTpFilterId(json.getLong("exportTpFilterId"));
				result.setTestCaseIssueTypeId(json.getString("testCaseIssueTypeId"));
				result.setTestPlanIssueTypeId(json.getString("testPlanIssueTypeId"));
				result.setTestCaseClosedStatusIds(json.getString("testPlanIssueTypeId"));
			} catch (JSONException e) {
				log.error(e.getMessage());
			}
		}
		return result;
	}

	@Override
	public List<String> getAdditionalTestPlanIssueTypes(Long projectId) {
		List<String> result = null;

		String executeRestCall = executeRestCall("/rest/test-flo/1.0/configuration/additionalTPs?projectId=" + projectId);
		if (StringUtils.isNotBlank(executeRestCall)) {
			String[] splitted = executeRestCall.split(";");
			result = Arrays.asList(splitted);
		}

		return result;
	}

	@Override
	public CustomField getStepsCf() {
		String executeRestCall = executeRestCall("/rest/test-flo/1.0/configuration/stepsCf");
		if (StringUtils.isNotBlank(executeRestCall)) {
			CustomField customFieldObject = customFieldManager.getCustomFieldObject(executeRestCall);
			return customFieldObject;
		}
		return null;
	}

	@Override
	public CustomField getRequirementCf() {
		String executeRestCall = executeRestCall("/rest/test-flo/1.0/configuration/requirementCf");
		if (StringUtils.isNotBlank(executeRestCall)) {
			CustomField customFieldObject = customFieldManager.getCustomFieldObject(executeRestCall);
			return customFieldObject;
		}
		return null;
	}

	private String getBaseUrl() {
		return ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
	}

	@Override
	public List<CustomField> getProgressCustomFields(Issue sampleTP) {
		List<CustomField> result = new ArrayList<CustomField>();
		if (sampleTP == null) {
			return result;
		}
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		String executeRestCall = executeRestCall("/rest/test-flo/1.0/configuration/progressCustomFields?issueKey=" + sampleTP.getKey());
		if (StringUtils.isNotBlank(executeRestCall)) {
			String[] split = executeRestCall.split(",");
			for (String string : split) {
				if (StringUtils.isNumeric(string)) {
					Long id = Long.valueOf(string);
					result.add(customFieldManager.getCustomFieldObject(id));
				}
			}
		}
		return result;
	}

	@Override
	public SubTasksColorStatusCFConfigBean getSubtasksColorStatusCfConfig(Long idAsLong, Long id, Issue issue) {
		Long projectId = issue.getProjectId();
		return getSubtasksColorStatusCfConfig(idAsLong, id, projectId);
	}

	@Override
	public SubTasksColorStatusCFConfigBean getSubtasksColorStatusCfConfig(Long idAsLong, Long id, Long projectId) {
		SubTasksColorStatusCFConfigBean result = new SubTasksColorStatusCFConfigBean();
		String executeRestCall = executeRestCall("/rest/test-flo/1.0/configuration/subtasksColorStatusCfConfig?fieldId=" + idAsLong + "&configId=" + id + "&projectId=" + projectId);
		if (StringUtils.isNotBlank(executeRestCall)) {
			JSONObject json;
			try {
				json = new JSONObject(executeRestCall);

				result.setChildIssueKeyCfId(json.has("childIssueKeyCfId") ? json.getString("childIssueKeyCfId") : "");
				result.setIssueColorStatusCfId(json.has("issueColorStatusCfId") ? json.getLong("issueColorStatusCfId") : -1L);
				result.setFinishedStatusIds(json.has("finishedStatusIds") ? json.getString("finishedStatusIds") : "");
				result.setIssueTypeIds(json.has("issueTypeIds") ? json.getString("issueTypeIds") : "");
				result.setUseDefaultStatusColor(json.has("useDefaultStatusColor") ? json.getString("useDefaultStatusColor") : "");
				result.setUseSubtasks(json.has("useSubtasks") ? json.getString("useSubtasks") : "");
				result.setStatusColorMap(json.has("statusColorMap") ? json.getString("statusColorMap") : "");
			} catch (JSONException e) {
				log.error(e.getMessage());
			}
		}
		return result;
	}

	@Override
	public boolean testConnection() {
		if (getConfiguration() != null) {
			return true;
		}
		return false;
	}
}
