package com.intenso.jira.plugins.tms.reports.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.lucene.search.FieldComparatorSource;
import org.apache.lucene.search.SortField;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.plugin.report.Report;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.util.json.JsonUtil;
import com.atlassian.jira.util.velocity.NumberTool;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.component.TableLayoutUtils;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.query.Query;
import com.intenso.jira.plugins.tms.export.rest.RestCallService;
import com.intenso.jira.plugins.tms.export.rest.TmsConfigurationBean;

/**
 * @author InTENSO
 *
 */
public class TPProgressReportAction extends JiraWebActionSupport implements Report {

	private static final long serialVersionUID = -4090018717685055656L;
	private static final String PROJECT_PREFIX = "project";
	private static final String FILTER_PREFIX = "filter";
	private static final String DEFAULT_COLOR_KEY = "default_color";

	private final RestCallService restCallService;
	private final TableLayoutUtils tableLayoutUtils;
	private List<Issue> testPlans;

	private List<String> selectedTestPlans;

	private String tableHtml;
	private String infoMessage;
	private String projectOrFilterId;
	private String projectOrFilterName;
	private HashMap<CustomField, Double> progress;
	private NumberTool numberTool = new NumberTool(getLocaleForLoggedInUser());
	private String chartProgressJson;
	private String stackedChartProgressJson;
	private String stackedGraphProgressJson;
	private String confStringJson;
	private String filterError;
	private Integer error;

	public TPProgressReportAction(RestCallService restCallService, TableLayoutUtils tableLayoutUtils) {
		this.restCallService = restCallService;
		this.tableLayoutUtils = tableLayoutUtils;
	}

	private Long getProjectOrFilerIdLong() {
		Long filterId = null;
		if (projectOrFilterId != null && projectOrFilterId.startsWith(FILTER_PREFIX)) {
			filterId = Long.parseLong(projectOrFilterId.replace(FILTER_PREFIX + "-", ""));
		}
		return filterId;

	}

	private Locale getLocaleForLoggedInUser() {
		return ComponentAccessor.getJiraAuthenticationContext().getLocale();
	}
	
	private I18nHelper getI18nHelperForLoggedInUser() {
		return ComponentAccessor.getJiraAuthenticationContext().getI18nHelper();
	}
	
	@Override
	protected void doValidation() {
		if (projectOrFilterId == null || projectOrFilterId.isEmpty())
			addErrorMessage(getI18nHelperForLoggedInUser().getText("com.intenso.suitest.filter_required"));
		if (!projectOrFilterId.startsWith(PROJECT_PREFIX)) {

			SearchRequest sr = ComponentAccessor.getComponent(SearchRequestManager.class).getSearchRequestById(getProjectOrFilerIdLong());
			Query query = sr.getQuery();
			QueryVisitor qv = new QueryVisitor();
			if (query.getWhereClause() == null)
				addErrorMessage(getI18nHelperForLoggedInUser().getText("testflo.reports.project_not_set_in_filter"));
			else {
				query.getWhereClause().accept(qv);

				if (!qv.getProjectIsSet()) {
					addErrorMessage(getI18nHelperForLoggedInUser().getText("testflo.reports.project_not_set_in_filter"));
				}

				if (qv.getProjectClauseCount() > 1) {
					addErrorMessage(getI18nHelperForLoggedInUser().getText("testflo.reports.more_than_one_project_set_in_filter"));
				}
			}
		}

		setFilterName();
	}

	private void setFilterName() {
		if (projectOrFilterId != null) {

			if (projectOrFilterId.startsWith(FILTER_PREFIX)) {
				SearchRequest sr = ComponentAccessor.getComponent(SearchRequestManager.class).getSearchRequestById(getProjectOrFilerIdLong());
				projectOrFilterName = sr.getName();
			} else if (projectOrFilterId.startsWith(PROJECT_PREFIX)) {
				ProjectManager pm = ComponentAccessor.getComponent(ProjectManager.class);
				Project po = pm.getProjectObj(Long.parseLong(projectOrFilterId.replaceAll(PROJECT_PREFIX + "-", "")));
				projectOrFilterName = po.getName();
			}
		}

	}

	@Override
	public String doDefault() throws Exception {
		if (error != null && error > 0)
			filterError = "com.intenso.suitest.filter_required";
		setFilterName();
		return super.doDefault();
	}

	private HashMap<String, Long> countByType(List<Issue> testPlans, TmsConfigurationBean configuration) {

		String tcIdFromConfig = configuration.getTestCaseIssueTypeId();

		HashMap<String, Long> counter = new HashMap<String, Long>();

		if (testPlans != null)
			for (Issue tp : testPlans) {
				Collection<Issue> tcs = tp.getSubTaskObjects();
				for (Issue tc : tcs) {
					IssueType it = tc.getIssueType();
					String name = it.getId();
					String id = tcIdFromConfig;

					if (name.equals(id)) {
						String status = tc.getStatus().getName();
						if (counter.containsKey(status)) {
							long old = counter.get(status);
							counter.put(status, old + 1);
						} else
							counter.put(status, 1l);

					}

				}
			}

		return counter;

	}

	/**
	 * Assumption: only one project in filter allowed, otherwise there is huge
	 * problem with per project color configuration
	 */
	@Override
	protected String doExecute() throws Exception {

		TmsConfigurationBean configuration = restCallService.getConfiguration();
		if (configuration == null) {
			log.error("TestFLO - Reports cannot get configuration from TestFLO for Test Management plugin.");
			filterError = "TestFLO - Reports cannot communicates with TestFLO for Test Management plugin.";
			return super.doDefault();
		}
		if (projectOrFilterId == null || projectOrFilterId.isEmpty())
			return getRedirect("/secure/analytics!default.jspa?error=1");

		HashMap<CustomField, HashMap<String, Long>> counter = new HashMap<CustomField, HashMap<String, Long>>();// CF,<status,count>>
		HashMap<CustomField, LinkedHashMap<String, String>> colors = new HashMap<CustomField, LinkedHashMap<String, String>>();
		HashMap<CustomField, SubTasksColorStatusCFConfigBean> configs = new HashMap<CustomField, SubTasksColorStatusCFConfigBean>();
		HashSet<Long> configIds = new HashSet<Long>();

		testPlans = getTestPlansBy(projectOrFilterId, configuration);

		/**************************** Second chart which present statuses for each progress bar ***************************/
		// counter (<progress field, <status, value>)
		// colors (<progress field, <status,color>)

		Issue sampleTP = null;

		if (testPlans != null && testPlans.size() > 0) {
			sampleTP = testPlans.get(0);
		}

		List<CustomField> progressCf = new ArrayList<CustomField>();
		if (sampleTP != null) {
			progressCf = restCallService.getProgressCustomFields(sampleTP);
		}

		for (CustomField prCf : progressCf) {
			FieldConfig fcc = getConfigForCf(prCf, sampleTP.getProjectId());

			SubTasksColorStatusCFConfigBean cfg = null;
			if (fcc != null) {
				cfg = restCallService.getSubtasksColorStatusCfConfig(prCf.getIdAsLong(), fcc.getId(), sampleTP);
			}

			if (cfg != null) {
				String statusColorMap = cfg.getStatusColorMap();
				LinkedHashMap<String, String> statusColors = new LinkedHashMap<String, String>();
				StatusManager sm = ComponentAccessor.getComponent(StatusManager.class);
				LinkedHashMap<String, String> statusColorsByName = new LinkedHashMap<String, String>();
				if (statusColorMap != null) {
					String[] pairs = statusColorMap.split(",");
					for (String pair : pairs) {
						String[] pairArr = pair.split("=");
						if (pairArr.length == 2) {
							String key;
							if (pairArr[0].equals("default_color")) {
								key = pairArr[0];
							} else {
								key = sm.getStatus(pairArr[0]).getName();
							}
							String value = pairArr[1];
							statusColorsByName.put(key, value);
						}
					}
				}
				statusColors = statusColorsByName;
				colors.put(prCf, statusColors);
			}

			HashMap<String, Long> partialResult = countByType(testPlans, configuration);
			configIds.add(cfg.getIssueColorStatusCfId());
			configs.put(prCf, cfg);
			counter.put(prCf, partialResult);

		}

		// Preparing data to display

		JSONArray totalData = new JSONArray();
		for (Long conf : configIds) {

			JSONArray ja2 = new JSONArray();
			JSONObject jconf = new JSONObject();
			jconf.put("conf", conf);
			ja2.put(jconf);
			for (CustomField cf : counter.keySet()) {
				Long issueColorStatusCfId = configs.get(cf).getIssueColorStatusCfId();
				if (issueColorStatusCfId != null && issueColorStatusCfId.equals(conf)) {
					JSONObject jo = getStackedChartData(cf, counter.get(cf));
					ja2.put(jo);
				}
			}

			totalData.put(ja2);

		}

		stackedChartProgressJson = JsonUtil.toJsonString(totalData);

		// Get all statuses for configuration <config id, <status>>
		HashMap<Long, HashMap<String, String>> statusByConfig = new HashMap<Long, HashMap<String, String>>();

		for (CustomField cfFromCounter : counter.keySet()) {

			HashMap<String, Long> resultForCF = counter.get(cfFromCounter);
			if (resultForCF == null) {
				continue;
			}
			Long confId = configs.get(cfFromCounter).getIssueColorStatusCfId();
			if (confId == null) {
				continue;
			}
			LinkedHashMap<String, String> linkedHashMap = colors.get(cfFromCounter);
			if (linkedHashMap == null) {
				continue;
			}
			if (statusByConfig.containsKey(confId)) {
				HashMap<String, String> ss = statusByConfig.get(confId);
				if (ss == null) {
					ss = new HashMap<String, String>();
				}

				for (String s : resultForCF.keySet()) {
					String preparedColor = linkedHashMap.get(s);
					if (preparedColor == null) {
						preparedColor = linkedHashMap.get(DEFAULT_COLOR_KEY);// set
																				// default
					}
					ss.put(s, preparedColor);
				}
				statusByConfig.put(confId, ss);

			} else {
				HashMap<String, String> ss = new HashMap<String, String>();
				for (String s : resultForCF.keySet()) {
					String preparedColor = linkedHashMap.get(s);
					if (preparedColor == null) {
						preparedColor = linkedHashMap.get(DEFAULT_COLOR_KEY);// set
																				// default
					}
					ss.put(s, preparedColor);
				}
				statusByConfig.put(confId, ss);

			}

		}

		// prepare graph legend

		JSONArray graphLegend = prepareLegend(statusByConfig);
		stackedGraphProgressJson = JsonUtil.toJsonString(graphLegend);// it will
																		// be as
																		// many
																		// charts
																		// as
																		// configs,
																		// so
																		// it's
																		// array
																		// of
																		// array

		// prepare conf data
		JSONArray confs = new JSONArray();
		for (Long l : configIds)
			confs.put(l);
		setConfStringJson(JsonUtil.toJsonString(confs));

		/******************************** First chart which present summary progress for custom fields *********************/

		if (testPlans.size() == 0) {
			return super.doExecute();
		}
		List<CustomField> progressCustomFields = restCallService.getProgressCustomFields(testPlans.get(0));
		progress = new LinkedHashMap<CustomField, Double>();
		for (CustomField customField : progressCustomFields) {
			Double sum = 0d;
			Integer count = 0;

			for (Issue tp : testPlans) {
				try {
					Object val = tp.getCustomFieldValue(customField);
					sum += val == null ? 0d : (val.toString().isEmpty() ? 0 : Double.valueOf(val.toString()));
					count++;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			progress.put(customField, count == 0 ? 0d : sum / count);
		}
		JSONArray ja1 = new JSONArray();

		for (CustomField cf : progress.keySet()) {

			JSONObject o = new JSONObject();
			o.put("category", cf.getName());
			o.put("progress", Math.round((progress.get(cf) * 100)) / 100.0);
			ja1.put(o);

		}
		chartProgressJson = JsonUtil.toJsonString(ja1);

		/*****************************************************************************************************************/

		return super.doExecute();

	}

	/*
	 * Get applicable scheme if project is specified or global context if not
	 */

	private FieldConfig getConfigForCf(CustomField cf, Long projectId) {

		FieldConfigScheme projectContext = null;
		FieldConfigScheme globalContext = null;

		for (FieldConfigScheme fcScheme : ComponentAccessor.getComponent(FieldConfigSchemeManager.class).getConfigSchemesForField(cf)) {

			if (fcScheme.isAllProjects())// global context
				globalContext = fcScheme;

			for (Project p : fcScheme.getAssociatedProjectObjects()) {
				if (p.getId().equals(projectId))
					projectContext = fcScheme;
			}
		}

		FieldConfigScheme properScheme = null;

		if (projectContext != null)
			properScheme = projectContext;
		else if (globalContext != null)
			properScheme = globalContext;
		else
			return null;

		Collection<FieldConfig> fci = properScheme.getConfigs().values();
		for (FieldConfig f : fci) {
			return f;

		}

		return null;

	}

	private JSONArray prepareLegend(HashMap<Long, HashMap<String, String>> statusByConfig) throws JSONException {

		JSONArray result = new JSONArray();

		for (Long confId : statusByConfig.keySet()) {

			JSONArray ja = new JSONArray();
			JSONObject jconf = new JSONObject();
			jconf.put("conf", confId);
			ja.put(jconf);
			HashMap<String, String> statusAndColor = statusByConfig.get(confId);
			for (String statusInConfig : statusAndColor.keySet()) {

				String status = statusInConfig;
				String color = statusAndColor.get(statusInConfig);
				JSONObject jo = new JSONObject();
				jo.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]%</b></span>");
				jo.put("fillAlphas", 1.0);
				jo.put("labelText", "[[value]]%");
				jo.put("lineAlpha", 0.3);
				jo.put("title", status);
				jo.put("type", "column");
				jo.put("color", "#000000");
				jo.put("valueField", status.replaceAll("\\s", "").toLowerCase());
				jo.put("fillColors", "#" + color);
				ja.put(jo);

			}

			result.put(ja);

		}

		return result;

	}

	private JSONObject getStackedChartData(CustomField cf, HashMap<String, Long> counter) throws JSONException {

		JSONObject jx = new JSONObject();
		jx.put("field_name", cf.getName());

		Long total = 0l;
		for (String s : counter.keySet()) {
			total += counter.get(s);
		}

		for (String s : counter.keySet()) {
			jx.put(s.replaceAll("\\s", "").toLowerCase(), Math.round(counter.get(s) * 10000.0 / total) / 100.0);

		}

		return jx;

	}

	private List<Issue> getTestPlansBy(String projectOrFilterId, TmsConfigurationBean configuration) throws SearchException, JqlParseException {
		SearchProvider searchProvider = ComponentAccessor.getOSGiComponentInstanceOfType(SearchProvider.class);
		if (projectOrFilterId != null && projectOrFilterId.startsWith(PROJECT_PREFIX)) {
			JqlQueryParser jqlQueryParser = ComponentAccessor.getOSGiComponentInstanceOfType(JqlQueryParser.class);
			Long projectId = Long.parseLong(projectOrFilterId.replace(PROJECT_PREFIX + "-", ""));
			Project project = ComponentAccessor.getProjectManager().getProjectObj(projectId);
			Query parseQuery = jqlQueryParser.parseQuery("project = " + project.getKey() + " AND issuetype = " + configuration.getTestPlanIssueTypeId());
			SearchResults search = searchProvider.search(parseQuery, getLoggedInUser(), PagerFilter.getUnlimitedFilter());
			return search.getIssues();
		} else if (projectOrFilterId != null && projectOrFilterId.startsWith(FILTER_PREFIX)) {
			Long filterId = Long.parseLong(projectOrFilterId.replace(FILTER_PREFIX + "-", ""));
			SearchRequestManager searchRequestManager = ComponentAccessor.getOSGiComponentInstanceOfType(SearchRequestManager.class);
			SearchRequest searchRequestById = searchRequestManager.getSearchRequestById(filterId);
			SearchResults search = searchProvider.search(searchRequestById.getQuery(), getLoggedInUser(), PagerFilter.getUnlimitedFilter());
			return search.getIssues();

		} else {
			return new ArrayList<Issue>();

		}

	}

	public TableLayoutUtils getTableLayoutUtils() {
		return tableLayoutUtils;
	}

	public List<Issue> getTestPlans() {
		return testPlans;
	}

	public void setTestPlans(List<Issue> testPlans) {
		this.testPlans = testPlans;
	}

	public List<String> getSelectedTestPlans() {
		return selectedTestPlans;
	}

	public void setSelectedTestPlans(List<String> selectedTestPlans) {
		this.selectedTestPlans = selectedTestPlans;
	}

	public String getTableHtml() {
		return tableHtml;
	}

	public void setTableHtml(String tableHtml) {
		this.tableHtml = tableHtml;
	}

	public String getInfoMessage() {
		return infoMessage;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getProjectOrFilterId() {
		return projectOrFilterId;
	}

	public void setProjectOrFilterId(String projectOrFilterId) {
		this.projectOrFilterId = projectOrFilterId;
	}

	public HashMap<CustomField, Double> getProgress() {
		return progress;
	}

	public void setProgress(HashMap<CustomField, Double> progress) {
		this.progress = progress;
	}

	public NumberTool getNumberTool() {
		return numberTool;
	}

	public void setNumberTool(NumberTool numberTool) {
		this.numberTool = numberTool;
	}

	public String getChartProgressJson() {
		return chartProgressJson;
	}

	public void setChartProgressJson(String chartProgressJson) {
		this.chartProgressJson = chartProgressJson;
	}

	public String getFilterError() {
		return filterError;
	}

	public void setFilterError(String filterError) {
		this.filterError = filterError;
	}

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	public String getStackedChartProgressJson() {
		return stackedChartProgressJson;
	}

	public void setStackedChartProgressJson(String stackedChartProgressJson) {
		this.stackedChartProgressJson = stackedChartProgressJson;
	}

	public String getStackedGraphProgressJson() {
		return stackedGraphProgressJson;
	}

	public void setStackedGraphProgressJson(String stackedGraphProgressJson) {
		this.stackedGraphProgressJson = stackedGraphProgressJson;
	}

	public String getConfStringJson() {
		return confStringJson;
	}

	public void setConfStringJson(String confStringJson) {
		this.confStringJson = confStringJson;
	}

	public String getProjectOrFilterName() {
		return projectOrFilterName;
	}

	public void setProjectOrFilterName(String projectOrFilterName) {
		this.projectOrFilterName = projectOrFilterName;
	}

	public String getDemoImgUrl() {
		return ComponentAccessor.getWebResourceUrlProvider().getStaticPluginResourceUrl("com.intenso.jira.plugins.testFLO-reports:suitest-export-resources", "images/tp_progress.png", UrlMode.AUTO);
	}

	@Override
	public String generateReportExcel(ProjectActionSupport arg0, Map arg1) throws Exception {
		return null;
	}

	@Override
	public String generateReportHtml(ProjectActionSupport arg0, Map arg1) throws Exception {
		return getRedirect("/secure/TpProgressReport!default.jspa");
	}

	@Override
	public void init(ReportModuleDescriptor arg0) {
	}

	@Override
	public boolean isExcelViewSupported() {
		return false;
	}

	@Override
	public boolean showReport() {
		return true;
	}

	@Override
	public void validate(ProjectActionSupport arg0, Map arg1) {
	}

}

class EmptyNavigableField implements NavigableField {

	private final String id;

	public EmptyNavigableField(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getNameKey() {
		return null;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}

	@Override
	public String getColumnHeadingKey() {
		return null;
	}

	@Override
	public String getColumnCssClass() {
		return "";
	}

	@Override
	public String getDefaultSortOrder() {
		return null;
	}

	@Override
	public FieldComparatorSource getSortComparatorSource() {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public LuceneFieldSorter getSorter() {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String getColumnViewHtml(FieldLayoutItem fieldLayoutItem, Map displayParams, Issue issue) {
		return null;
	}

	@Override
	public String getHiddenFieldId() {
		return null;
	}

	@Override
	public String prettyPrintChangeHistory(String changeHistory) {
		return null;
	}

	@Override
	public String prettyPrintChangeHistory(String changeHistory, I18nHelper i18nHelper) {
		return null;
	}

	@Override
	public List<SortField> getSortFields(boolean sortOrder) {
		return null;
	}

}
