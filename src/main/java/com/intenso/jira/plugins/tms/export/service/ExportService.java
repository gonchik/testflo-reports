package com.intenso.jira.plugins.tms.export.service;

import java.io.IOException;

/**
 * @author InTENSO
 *
 */
public interface ExportService {
	
	String generateHtml(String issueKey, String urlQuery, String sourceIssue) throws IOException;
}
