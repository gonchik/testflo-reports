package com.intenso.jira.plugins.tms.export.condition;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.intenso.jira.plugins.tms.export.rest.RestCallService;
import com.intenso.jira.plugins.tms.export.rest.TmsConfigurationBean;

/**
 * @author InTENSO
 *
 */
public class TestCaseCondition extends AbstractWebCondition {

	private final RestCallService restCallService;

	public TestCaseCondition(RestCallService restCallService) {
		super();
		this.restCallService = restCallService;
	}

	@Override
	public boolean shouldDisplay(ApplicationUser user, JiraHelper helper) {
		Issue issue = (Issue) helper.getContextParams().get("issue");

		TmsConfigurationBean configuration = restCallService.getConfiguration();
		if (configuration == null) {
			return false;
		}
		if (issue.getIssueTypeId().equals(configuration.getTestCaseIssueTypeId())) {
			return true;
		}

		return false;
	}
}
