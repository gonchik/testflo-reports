package com.intenso.jira.plugins.tms.reports.integration.agile.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;

/**
 * @author InTENSO
 *
 */
public class AgileLoginFilter extends AbstractLoginFilter implements Filter {

	private Logger log = Logger.getLogger(this.getClass());
	public static final String AGILE_SUITEST_HEADER = "X-Suitest-Agile";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		String username = getUsername(request);
		Boolean suitestTask = Boolean.FALSE;
		if (username != null) {
			suitestTask = Boolean.TRUE;
			log.debug("Do internal login filter " + username);
		}
		if (username == null) {
			// basic authentication
			String[] basicAuthCredentials = getBasicAuthCredentials((HttpServletRequest) request);
			log.debug("InternalLoginFilter  " + (basicAuthCredentials != null ? "" : "not ") + "found in request");
			if (basicAuthCredentials != null && basicAuthCredentials.length == 2 && basicAuthCredentials[0] != null) {
				ApplicationUser userObj = ComponentAccessor.getUserManager().getUserByKey(basicAuthCredentials[0]);
				try {
					crowdServiceAuthenticate(userObj, basicAuthCredentials[1]);
					username = basicAuthCredentials[0];
				} catch (Exception e) {
					log.debug("InternalLoginFilter  Basic Authentication failed " + e.getMessage());
				}
			}

		}

		// perform login
		if (username != null) {
			ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(username);
			if (user != null) {
				putPrincipalInSessionContext((HttpServletRequest) request, user);
			}
		}

		// chain other filters anyway
		chain.doFilter(request, response);

		if (suitestTask) {
			// perform logout
			removePrincipalFromSessionContext((HttpServletRequest) request);
		}

	}

	private String getUsername(ServletRequest request) {
		String username = ((HttpServletRequest) request).getHeader(AGILE_SUITEST_HEADER);
		return username;
	}

	@Override
	public void destroy() {

	}

}
