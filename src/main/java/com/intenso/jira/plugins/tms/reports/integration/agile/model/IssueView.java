package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.intenso.jira.plugins.tms.reports.integration.agile.visitor.VisitableElement;
import com.intenso.jira.plugins.tms.reports.integration.agile.visitor.Visitor;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueView implements VisitableElement {

	@JsonProperty
	private Long id;
	@JsonProperty
	private String key;
	@JsonProperty
	private Boolean hidden;
	@JsonProperty
	private String typeName;
	@JsonProperty
	private String typeId;
	@JsonProperty
	private String summary;
	@JsonProperty
	private String typeUrl;
	@JsonProperty
	private String priorityUrl;
	@JsonProperty
	private String priorityName;
	@JsonProperty
	private Boolean done;
	@JsonProperty
	private String assignee;
	@JsonProperty
	private String assigneeName;
	@JsonProperty
	private String avatarUrl;
	@JsonProperty
	private Boolean hasCustomUserAvatar;
	@JsonProperty
	private String color;
	@JsonProperty
	private EstimateStatistic estimateStatistic;
	@JsonProperty
	private TrackingStatistic trackingStatistic;
	@JsonProperty
	private String statusId;
	@JsonProperty
	private String statusName;
	@JsonProperty
	private String statusUrl;
	@JsonProperty
	private Long projectId;
	@JsonProperty
	private Long linkedPagesCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Boolean getHidden() {
		return hidden;
	}

	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getTypeUrl() {
		return typeUrl;
	}

	public void setTypeUrl(String typeUrl) {
		this.typeUrl = typeUrl;
	}

	public String getPriorityUrl() {
		return priorityUrl;
	}

	public void setPriorityUrl(String priorityUrl) {
		this.priorityUrl = priorityUrl;
	}

	public String getPriorityName() {
		return priorityName;
	}

	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

	public Boolean getDone() {
		return done;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public Boolean getHasCustomUserAvatar() {
		return hasCustomUserAvatar;
	}

	public void setHasCustomUserAvatar(Boolean hasCustomUserAvatar) {
		this.hasCustomUserAvatar = hasCustomUserAvatar;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public EstimateStatistic getEstimateStatistic() {
		return estimateStatistic;
	}

	public void setEstimateStatistic(EstimateStatistic estimateStatistic) {
		this.estimateStatistic = estimateStatistic;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getStatusUrl() {
		return statusUrl;
	}

	public void setStatusUrl(String statusUrl) {
		this.statusUrl = statusUrl;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getLinkedPagesCount() {
		return linkedPagesCount;
	}

	public void setLinkedPagesCount(Long linkedPagesCount) {
		this.linkedPagesCount = linkedPagesCount;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visitElement(this);
	}

	public TrackingStatistic getTrackingStatistic() {
		return trackingStatistic;
	}

	public void setTrackingStatistic(TrackingStatistic trackingStatistic) {
		this.trackingStatistic = trackingStatistic;
	}

}
