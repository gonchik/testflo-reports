package com.intenso.jira.plugins.tms.export.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutStorageException;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.component.TableLayoutFactory;
import com.atlassian.query.Query;
import com.intenso.jira.plugins.tms.export.rest.RestCallService;
import com.intenso.jira.plugins.tms.export.rest.TmsConfigurationBean;

/**
 * @author InTENSO
 *
 */
public class ExportServiceImpl implements ExportService {

	private final static Logger log = LoggerFactory.getLogger(ExportServiceImpl.class);
	private final static List<String> listOfProblematicStrings = createListOfProblematicStringRegexps();
	private final static Map<String, String> transformerStepsCf = createTransformerForStepsCf();
	
	private final SearchRequestManager searchRequestManager;
	private final TableLayoutFactory tableLayoutFactory;
	private final RestCallService restCallService;
	
	public ExportServiceImpl(SearchRequestManager searchRequestManager, RestCallService restCallService) {
		this.searchRequestManager = searchRequestManager;
		this.restCallService = restCallService;
		this.tableLayoutFactory = ComponentAccessor.getComponent(TableLayoutFactory.class);
	}

	@Override
	public String generateHtml(String issueKey, String urlQuery, String sourceIssue) throws IOException {
		StringBuilder stringBuffer = new StringBuilder();
		ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
		List<ColumnLayoutItem> columns = null;
		List<ColumnLayoutItem> columnsTc = null;
		List<ColumnLayoutItem> columnsTp = null;
		TmsConfigurationBean globalConfiguration = restCallService.getConfiguration();
		if (globalConfiguration != null) {
			Long filterId = globalConfiguration.getExportFilterId();
			Long tcFilterId = globalConfiguration.getExportTcFilterId();
			Long tpFilterId = globalConfiguration.getExportTpFilterId();
			columns = getColumnsFromFilter(loggedInUser, filterId);
			columnsTc = getColumnsFromFilter(loggedInUser, tcFilterId);
			columnsTp = getColumnsFromFilter(loggedInUser, tpFilterId);
		}
		if (issueKey != null) {
			IssueManager issueManager = ComponentAccessor.getIssueManager();
			Issue issue = issueManager.getIssueObject(issueKey);
			if (issue != null) {
				try {
					addIssueToView(issue, globalConfiguration, columnsTc, columnsTp, columns, stringBuffer);
					addSubtasksToView(issue, globalConfiguration, columnsTc, columnsTp, columns, stringBuffer);
				} catch (ColumnLayoutStorageException e) {
					log.error(e.getMessage());
				}
				stringBuffer.append("<style>.aui{ border-collapse: collapse;}.aui td, .aui th {border: 1px solid #aaa;}.visible-status-label {background-color: white !important;}</style>");
				stringBuffer.append("</body></html>");
			} else {
				log.error("Issue not found! (" + issueKey + ")");
			}
		} else if (urlQuery != null) {
			String jql = urlQuery.replace("jql=", "");
			JqlQueryParser jqlQueryParser = ComponentAccessor.getOSGiComponentInstanceOfType(JqlQueryParser.class);
			SearchProvider searchProvider = ComponentAccessor.getOSGiComponentInstanceOfType(SearchProvider.class);
			try {
				Query parsedQuery = jqlQueryParser.parseQuery(jql);
				SearchResults search = searchProvider.search(parsedQuery, ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), PagerFilter.getUnlimitedFilter());
				List<Issue> issues = search.getIssues();
				if (sourceIssue != null) {
					Issue issueObject = ComponentAccessor.getIssueManager().getIssueObject(sourceIssue);
					addIssueToView(issueObject, globalConfiguration, columnsTc, columnsTp, columns, stringBuffer);
				}
				for (Issue issueObject : issues) {
					addIssueToView(issueObject, globalConfiguration, columnsTc, columnsTp, columns, stringBuffer);
					addSubtasksToView(issueObject, globalConfiguration, columnsTc, columnsTp, columns, stringBuffer);
				}
				stringBuffer.append("<style>.aui{ border-collapse: collapse;}.aui td, .aui th {border: 1px solid #aaa;}.visible-status-label {background-color: white !important;}</style>");
			} catch (JqlParseException e) {
				log.error(e.getMessage());
			} catch (SearchException e) {
				log.error(e.getMessage());
			} catch (ColumnLayoutStorageException e) {
				log.error(e.getMessage());
			}
		} else {
			log.error("Issue key is null!");
		}
		return stringBuffer.toString();
	}
	
	private List<ColumnLayoutItem> getColumnsFromFilter(ApplicationUser loggedInUser, Long filterId) {
		List<ColumnLayoutItem> columns = new ArrayList<ColumnLayoutItem>();
		if (filterId != null) {
			SearchRequest searchRequest = searchRequestManager.getSearchRequestById(filterId);
			if (searchRequest != null) {
				columns.addAll(tableLayoutFactory.getStandardLayout(searchRequest, loggedInUser ).getColumns());
			}
		}
		return columns;
	}

	private void addSubtasksToView(Issue issue, TmsConfigurationBean globalConfiguration, List<ColumnLayoutItem> columnsTc, List<ColumnLayoutItem> columnsTp, List<ColumnLayoutItem> columns, StringBuilder stringBuffer) throws IOException, ColumnLayoutStorageException {
		stringBuffer.append("<div class='subtasks' style='margin-left: 30px;'>");
		for (Issue subtask : issue.getSubTaskObjects()) {
			addIssueToView(subtask, globalConfiguration, columnsTc, columnsTp, columns, stringBuffer);
		}
		stringBuffer.append("</div>");
	}

	private void addIssueToView(Issue issue, TmsConfigurationBean globalConfiguration, List<ColumnLayoutItem> columnsTc, List<ColumnLayoutItem> columnsTp, List<ColumnLayoutItem> columns, StringBuilder stringBuffer) throws IOException, ColumnLayoutStorageException {
		stringBuffer.append("<h2 style='font-family: arial;'>" + (issue.getParentObject() == null ? "" : "<a href=\"#\">" + issue.getParentObject().getKey() + "</a> / ") + issue.getSummary() + "</h2>");
		stringBuffer.append("<table style='font-family: arial;'>");
		String issueTypeId = issue.getIssueTypeId();
		List<ColumnLayoutItem> selectedColumns = new ArrayList<ColumnLayoutItem>();

		if (globalConfiguration == null) {
			printBasicIssueDetails(issue, stringBuffer);
		} else {
			if (issueTypeId.equals(globalConfiguration.getTestCaseIssueTypeId())) {
				selectedColumns.addAll(columnsTc);
			} else if (issueTypeId.equals(globalConfiguration.getTestPlanIssueTypeId())
					|| restCallService.getAdditionalTestPlanIssueTypes(issue.getProjectObject().getId()).contains(issueTypeId)){
				selectedColumns.addAll(columnsTp);
			} else {
				selectedColumns.addAll(columns);
			}
			for (ColumnLayoutItem columnLayoutItem : selectedColumns) {
				stringBuffer.append("<tr>");
				NavigableField navigableField = columnLayoutItem.getNavigableField();
				String name = navigableField.getName();
				CustomField stepsCf = restCallService.getStepsCf();
				CustomField requirementCf = restCallService.getRequirementCf();
				stringBuffer.append("<td>" + name + "</td>");
				if (stepsCf.getName().equals(name)) {
					String rawHtml = stepsCf.getViewHtml(ComponentAccessor.getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem(stepsCf), null, issue);
					String transformedHtml = transform(rawHtml, transformerStepsCf);
					stringBuffer.append("<td>" + transformedHtml + "</td>");
				} else if (requirementCf.getName().equals(name)) {
					stringBuffer.append("<td>" + requirementCf.getViewHtml(ComponentAccessor.getFieldLayoutManager().getFieldLayout(issue).getFieldLayoutItem(requirementCf), null, issue) + "</td>");
				} else if ("Issue Type".equals(name)) {
					String issueTypeName = issue.getIssueType().getName();
					stringBuffer.append("<td>" + issueTypeName + "</td>");
				} else {
					String row = removeTags(columnLayoutItem.getHtml(new HashMap<Object, Object>(), issue));
					stringBuffer.append("<td>" + row + "</td>");
				}
				stringBuffer.append("</tr>");
			}
		}
		stringBuffer.append("</table>");
		stringBuffer.append("<hr></hr>");
	}

	private void printBasicIssueDetails(Issue issue, StringBuilder stringBuffer) {
		stringBuffer.append("<tr>");
		stringBuffer.append("<td>Key</td>");
		stringBuffer.append("<td>" + issue.getKey() + "</td>");
		stringBuffer.append("</tr>");
		stringBuffer.append("<tr>");
		stringBuffer.append("<td>Summary</td>");
		stringBuffer.append("<td>" + issue.getSummary() + "</td>");
		stringBuffer.append("</tr>");
		stringBuffer.append("<tr>");
		stringBuffer.append("<td>Type</td>");
		stringBuffer.append("<td>" + issue.getIssueType().getName() + "</td>");
		stringBuffer.append("</tr>");
		stringBuffer.append("<tr>");
		stringBuffer.append("<td>Assignee</td>");
		ApplicationUser assignee = issue.getAssignee();
		if (assignee != null) {
			stringBuffer.append("<td>" + assignee.getDisplayName() + "</td>");
		} else {
			stringBuffer.append("<td></td>");
		}
		stringBuffer.append("</tr>");
		stringBuffer.append("<tr>");
		stringBuffer.append("<td>Status</td>");
		stringBuffer.append("<td>" + issue.getStatus().getName() + "</td>");
		stringBuffer.append("</tr>");
		stringBuffer.append("<tr>");
		stringBuffer.append("<td>Resolution</td>");
		Resolution resolution = issue.getResolution();
		if (resolution != null) {
			stringBuffer.append("<td>" + resolution.getName() + "</td>");
		} else {
			stringBuffer.append("<td></td>");
		}
		stringBuffer.append("</tr>");
	}

	private static Map<String, String> createTransformerForStepsCf() {
		Map<String, String> transformers = new HashMap<String, String>();

		// !!! the order of some transformers matters. If you don't what you're doing, go away.
		
		transformers.put("<th style=\"border-right: 1px solid #BBB\"></th>", "<th></th>");  // remove the unwanted border
		transformers.put("<th name", "<th style=\"border: 1px solid #BBB\" name"); 			// add borders where they're not present in headers
		transformers.put("border-right: 1px solid #BBB;", ""); 								// remove right-borders
		transformers.put("<td style=\"", "<td style=\"border: 1px solid #BBB;"); 			// add borders where they're needed in table cells
		transformers.put("<td colspan=\"6\"", "<td style=\"border: 1px solid #BBB\" colspan=\"6\""); // add borders around step groups
		transformers.put("<img.+><./src>", "");												// delete icons, they're empty anyway
		transformers.put("<table class=\"aui\">", "<table cellspacing=\"0px\">");			// collapse borders as much as possible
		
		return transformers;
	}
	
	private static List<String> createListOfProblematicStringRegexps() {
		List<String> listOfProblematicStrings = new LinkedList<String>();

		// Due date
		listOfProblematicStrings.add("<time(\"[^\"]*\"|'[^']*'|[^'\">])*>");
		listOfProblematicStrings.add("</time>");
		
		// TP Progress
		listOfProblematicStrings.add(".border-bottom-left-radius:[0-9]*px;border-top-left-radius:[0-9]*px;-webkit-border-bottom-left-radius:[0-9]*px;-webkit-border-top-left-radius:[0-9]*px;");
		listOfProblematicStrings.add(".border-bottom-right-radius:[0-9]*px;border-top-right-radius:[0-9]*px;-webkit-border-bottom-right-radius:[0-9]*px;-webkit-border-top-rigth-radius:[0-9]*px");
		
		return listOfProblematicStrings;
	}
	
	/**
	 * Removes unwanted strings from the inputText. These strings cause problems while generating the PDF document. 
	 * @param inputText
	 * @param stringsToBeRemovedRegexp
	 * @return
	 */
	private String removeTags(String inputText) {
		for(String stringRegexp : listOfProblematicStrings) {
			inputText = inputText.replaceAll(stringRegexp, "");
		}
		
		return inputText;
	}
	
	private String transform(String html, Map<String, String> transformer) {
		for(String stringRegexp : transformer.keySet()) {
			html = html.replaceAll(stringRegexp, transformerStepsCf.get(stringRegexp));
		}
		
		return html;
	}
}
