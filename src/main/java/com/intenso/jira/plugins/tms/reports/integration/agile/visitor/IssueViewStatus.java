package com.intenso.jira.plugins.tms.reports.integration.agile.visitor;

/**
 * @author InTENSO
 *
 */
public enum IssueViewStatus {
	INCOMPLETED, COMPLETED;

}
