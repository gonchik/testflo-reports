package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueViewWrapper {
	@JsonProperty
	private ContentWrapper contents;
	@JsonProperty
	private Sprint sprint;

	public ContentWrapper getContents() {
		return contents;
	}

	public void setContents(ContentWrapper contents) {
		this.contents = contents;
	}

	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

}
