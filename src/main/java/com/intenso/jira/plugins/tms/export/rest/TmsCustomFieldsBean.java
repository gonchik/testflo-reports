package com.intenso.jira.plugins.tms.export.rest;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * @author InTENSO
 *
 */
public class TmsCustomFieldsBean {
	@JsonProperty
	private Long requirementCfId;
	@JsonProperty
	private Long stepsCfId;
	@JsonProperty
	private Long tpStatusCfId;
	@JsonProperty
	private Long tpProgressCfId;
	@JsonProperty
	private Long tcStatusCfId;
	@JsonProperty
	private Long tcGroupCfId;
	@JsonProperty
	private Long tctChangeAlertCfId;
	@JsonProperty
	private Long tcTemplatesCfId;
	@JsonProperty
	private Long tcTemplateCfId;
	@JsonProperty
	private Long stepProgressCfId;
	@JsonProperty
	private Long defectsCfId;
	@JsonProperty
	private Long defectsOnTpCfId;

	public void setRequirementCfId(Long cfId) {
		this.requirementCfId = cfId;
	}
	public void setStepsCfId(Long cfId) {
		this.stepsCfId = cfId;
	}
	public void setTpStatusCfId(Long cfId) {
		this.tpStatusCfId = cfId;
	}
	public void setTpProgressCfId(Long cfId) {
		this.tpProgressCfId = cfId;
	}
	public void setTcStatusCfId(Long cfId) {
		this.tcStatusCfId = cfId;
	}
	public void setTcGroupCfId(Long cfId) {
		this.tcGroupCfId = cfId;
	}
	public void setTctChangeAlertCfId(Long cfId) {
		this.tctChangeAlertCfId = cfId;
	}
	public void setTcTemplatesCfId(Long cfId) {
		this.tcTemplatesCfId = cfId;
	}
	public void setTcTemplateCfId(Long cfId) {
		this.tcTemplateCfId = cfId;
	}
	public void setStepsProgressCfId(Long cfId) {
		this.stepProgressCfId = cfId;
	}
	public void setDefectsCfId(Long cfId) {
		this.defectsCfId = cfId;
	}
	public void setDefectsOnTpCfId(Long cfId) {
		this.defectsOnTpCfId = cfId;
	}
	public Long getRequirementCfId() {
		return requirementCfId;
	}
	public Long getStepsCfId() {
		return stepsCfId;
	}
	public Long getTpStatusCfId() {
		return tpStatusCfId;
	}
	public Long getTpProgressCfId() {
		return tpProgressCfId;
	}
	public Long getTcStatusCfId() {
		return tcStatusCfId;
	}
	public Long getTcGroupCfId() {
		return tcGroupCfId;
	}
	public Long getTctChangeAlertCfId() {
		return tctChangeAlertCfId;
	}
	public Long getTcTemplatesCfId() {
		return tcTemplatesCfId;
	}
	public Long getTcTemplateCfId() {
		return tcTemplateCfId;
	}
	public Long getStepProgressCfId() {
		return stepProgressCfId;
	}
	public Long getDefectsCfId() {
		return defectsCfId;
	}
	public Long getDefectsOnTpCfId() {
		return defectsOnTpCfId;
	}
	
}
