package com.intenso.jira.plugins.tms.reports.integration.agile.visitor;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.intenso.jira.plugins.tms.export.rest.RestCallService;
import com.intenso.jira.plugins.tms.export.rest.TmsConfigurationBean;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueView;

/**
 * @author InTENSO
 *
 */
public abstract class AbstractBaseTestVisitor implements Visitor {

	private IssueManager issueManager;

	protected boolean isTestPlan(IssueView issueView) {
		Long id = issueView.getId();
		Issue issue = getIssueManager().getIssueObject(id);
		RestCallService restCallService = ComponentAccessor.getOSGiComponentInstanceOfType(RestCallService.class);
		TmsConfigurationBean configuration = restCallService.getConfiguration();
		String testPlanIssueTypeId = configuration.getTestPlanIssueTypeId();
		if (issue != null && issue.getIssueTypeId().equals(testPlanIssueTypeId)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public abstract void visitElement(IssueView issueView);


	public IssueManager getIssueManager() {
		if (issueManager == null) {
			issueManager = ComponentAccessor.getComponent(IssueManager.class);
		}
		return issueManager;
	}

}
