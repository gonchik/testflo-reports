package com.intenso.jira.plugins.tms.reports.integration.agile.filter;

import java.security.Principal;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.exception.FailedAuthenticationException;
import com.atlassian.jira.component.ComponentAccessor;

/**
 * @author InTENSO
 *
 */
public abstract class AbstractLoginFilter implements Filter {

	protected void putPrincipalInSessionContext(final HttpServletRequest httpServletRequest, final Principal principal) {
		HttpSession httpSession = httpServletRequest.getSession();
		httpSession.setAttribute("seraph_defaultauthenticator_user", principal);
		httpSession.setAttribute("seraph_defaultauthenticator_logged_out_user", null);
	}

	protected void removePrincipalFromSessionContext(HttpServletRequest httpServletRequest) {
		HttpSession httpSession = httpServletRequest.getSession();
		httpSession.setAttribute("seraph_defaultauthenticator_user", null);
		httpSession.setAttribute("seraph_defaultauthenticator_logged_out_user", Boolean.TRUE);
	}

	protected void crowdServiceAuthenticate(Principal user, String password) throws FailedAuthenticationException {
		final Thread currentThread = Thread.currentThread();
		final ClassLoader origCCL = currentThread.getContextClassLoader();
		try {
			currentThread.setContextClassLoader(this.getClass().getClassLoader());
			ComponentAccessor.getComponent(CrowdService.class).authenticate(user.getName(), password);
		} finally {
			currentThread.setContextClassLoader(origCCL);
		}
	}

	protected String[] getBasicAuthCredentials(final HttpServletRequest request) {
		String authHeader = request.getHeader("Authorization");

		if (authHeader != null && authHeader.substring(0, 5).equalsIgnoreCase("Basic")) {
			String base64Token = authHeader.substring(6);
			String token = new String(Base64.decodeBase64(base64Token.getBytes()));

			final int delim = token.indexOf(":");

			if (delim != -1) {
				String name = token.substring(0, delim);
				String password = token.substring(delim + 1);

				return new String[] { name, password };
			}
		}

		return null;
	}

}
