package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContentWrapper {

	@JsonProperty
	private IssueView[] completedIssues;
	@JsonProperty
	private IssueView[] incompletedIssues;
	@JsonProperty
	private IssueView[] puntedIssues;

	public IssueView[] getCompletedIssues() {
		return completedIssues;
	}

	public void setCompletedIssues(IssueView[] completedIssues) {
		this.completedIssues = completedIssues;
	}

	public IssueView[] getIncompletedIssues() {
		return incompletedIssues;
	}

	public void setIncompletedIssues(IssueView[] incompletedIssues) {
		this.incompletedIssues = incompletedIssues;
	}

	public IssueView[] getPuntedIssues() {
		return puntedIssues;
	}

	public void setPuntedIssues(IssueView[] puntedIssues) {
		this.puntedIssues = puntedIssues;
	}

}
