package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackingStatistic {

	@JsonProperty
	private String staticFieldId;
	@JsonProperty
	private TrackingValue statFieldValue;

	public String getStaticFieldId() {
		return staticFieldId;
	}

	public void setStaticFieldId(String staticFieldId) {
		this.staticFieldId = staticFieldId;
	}

	public TrackingValue getStatFieldValue() {
		return statFieldValue;
	}

	public void setStatFieldValue(TrackingValue statFieldValue) {
		this.statFieldValue = statFieldValue;
	}

}
