package com.intenso.jira.plugins.tms.reports.integration.agile.visitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.intenso.jira.plugins.tms.export.rest.RestCallService;
import com.intenso.jira.plugins.tms.export.rest.TmsConfigurationBean;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueView;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.TCObject;

/**
 * @author InTENSO
 *
 */
public class EstimateTestPlanVisitor extends AbstractBaseTestVisitor {

	private static final Logger log = LoggerFactory.getLogger(EstimateTestPlanVisitor.class);
	private Map<String, List<TCObject>> requirements = new HashMap<String, List<TCObject>>();

	@SuppressWarnings("unchecked")
	@Override
	public void visitElement(IssueView issueViewObject) {
		IssueViewDecorator issueView = (IssueViewDecorator) issueViewObject;
		if (issueView == null) {
			log.error("IssueViewObject is null!");
			return;
		}
		RestCallService restCallService = ComponentAccessor.getOSGiComponentInstanceOfType(RestCallService.class);
		TmsConfigurationBean config = restCallService.getConfiguration();
		CustomField requirementCF = restCallService.getRequirementCf();

		if (config != null && isTestPlan(issueView)) {

			Issue issue = getIssueManager().getIssueObject(issueView.getKey());
			Collection<Issue> subtasks = issue.getSubTaskObjects();

			for (Issue subtask : subtasks) {

				// get origin TCT
				String testCaseIssueType = config.getTestCaseIssueTypeId();
				if (subtask.getIssueTypeId().equals(testCaseIssueType) && requirementCF != null) {

					Collection<String> requirementsList = (Collection<String>) requirementCF.getCustomFieldType().getValueFromIssue(requirementCF, subtask);
					if (requirementsList != null) {
						for (String req : requirementsList) {

							if (requirements.get(req) == null) {
								requirements.put(req, new ArrayList<TCObject>());
							}
							String testCaseClosedStatusIds = config.getTestCaseClosedStatusIds();
							if (testCaseClosedStatusIds != null) {
								List<String> statuses = Arrays.asList(testCaseClosedStatusIds.split(","));
								String currentStatus = subtask.getStatus().getId();
								boolean wasTested = Boolean.FALSE;
								if (statuses != null) {
									for (String status : statuses) {
										if (status.equals(currentStatus)) {
											wasTested = Boolean.TRUE;
											break;
										}
									}
								}
								requirements.get(req).add(new TCObject(subtask.getId(), subtask.getKey(), wasTested));
							}
						}
					}
				}
			}
		} else {
			log.error("TestFLO - Reports cannot get configuration from TestFLO Test Management plugin.");
		}
	}

	public List<String> getSatisfiedRequirements() {
		List<String> satisfied = new ArrayList<String>();
		for (String reqKey : requirements.keySet()) {
			boolean isSatisfied = Boolean.TRUE;
			if (requirements.get(reqKey) != null) {
				for (TCObject tc : requirements.get(reqKey)) {
					if (tc.getResolution() != null && tc.getResolution().equals(Boolean.FALSE)) {
						isSatisfied = Boolean.FALSE;
						break;
					}
				}
			}

			if (isSatisfied) {
				satisfied.add(reqKey);
			}
		}

		return satisfied;
	}

	public List<String> getNotSatisfiedRequirements() {
		List<String> notSatisfied = new ArrayList<String>();
		for (String reqKey : requirements.keySet()) {
			boolean isSatisfied = Boolean.TRUE;
			if (requirements.get(reqKey) != null) {
				for (TCObject tc : requirements.get(reqKey)) {
					if (tc.getResolution() != null && tc.getResolution().equals(Boolean.FALSE)) {
						isSatisfied = Boolean.FALSE;
						break;
					}
				}
			}

			if (!isSatisfied) {
				notSatisfied.add(reqKey);
			}

		}

		return notSatisfied;
	}

}
