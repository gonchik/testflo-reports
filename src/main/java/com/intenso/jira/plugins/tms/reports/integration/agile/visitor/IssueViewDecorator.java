package com.intenso.jira.plugins.tms.reports.integration.agile.visitor;

import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueView;

/**
 * @author InTENSO
 *
 */
public class IssueViewDecorator extends IssueView {

	private IssueViewStatus status;

	public IssueViewDecorator(IssueView issueView, IssueViewStatus issueViewStatus) {
		this.setAssignee(issueView.getAssignee());
		this.setAssigneeName(issueView.getAssigneeName());
		this.setAvatarUrl(issueView.getAvatarUrl());
		this.setColor(issueView.getColor());
		this.setDone(issueView.getDone());
		this.setEstimateStatistic(issueView.getEstimateStatistic());
		this.setHasCustomUserAvatar(issueView.getHasCustomUserAvatar());
		this.setHidden(issueView.getHidden());
		this.setId(issueView.getId());
		this.setKey(issueView.getKey());
		this.setLinkedPagesCount(issueView.getLinkedPagesCount());
		this.setPriorityName(issueView.getPriorityName());
		this.setPriorityUrl(issueView.getPriorityUrl());
		this.setProjectId(issueView.getProjectId());
		this.setStatusId(issueView.getStatusId());
		this.setStatusName(issueView.getStatusName());
		this.setStatusUrl(issueView.getStatusUrl());
		this.setSummary(issueView.getSummary());
		this.setTypeId(issueView.getTypeId());
		this.setTypeName(issueView.getTypeName());
		this.setTypeUrl(issueView.getTypeUrl());
		this.setStatus(issueViewStatus);
		this.setTrackingStatistic(issueView.getTrackingStatistic());
	}

	public IssueViewStatus getStatus() {
		return status;
	}

	public void setStatus(IssueViewStatus status) {
		this.status = status;
	}

}
