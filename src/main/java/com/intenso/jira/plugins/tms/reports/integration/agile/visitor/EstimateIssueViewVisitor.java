package com.intenso.jira.plugins.tms.reports.integration.agile.visitor;

import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueView;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.TrackingValue;

/**
 * @author InTENSO
 *
 */
public class EstimateIssueViewVisitor implements Visitor {

	private Double totalEstimate = 0d;

	@Override
	public void visitElement(IssueView issueView) {

		if (issueView != null && issueView.getEstimateStatistic() != null) {
			TrackingValue trackingValue = issueView.getEstimateStatistic().getStatFieldValue();
			if (trackingValue != null) {
				Long value = trackingValue.getValue();
				if (value != null)
					totalEstimate += new Double(value);
			}
		}

	}

	public Double getTotalEstimate() {
		return totalEstimate;
	}

	public void setTotalEstimate(Double totalEstimate) {
		this.totalEstimate = totalEstimate;
	}

}
