package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SprintWrapper {

	@JsonProperty
	private Sprint[] sprints;

	public Sprint[] getSprints() {
		return sprints;
	}

	public void setSprints(Sprint[] sprints) {
		this.sprints = sprints;
	}

}
