package com.intenso.jira.plugins.tms.reports.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.report.Report;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.google.gson.Gson;
import com.intenso.jira.plugins.tms.export.rest.RestCallService;
import com.intenso.jira.plugins.tms.reports.integration.agile.AgileFascade;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.Sprint;

/**
 * @author InTENSO
 *
 */
public class AgileProgressReportAction extends JiraWebActionSupport implements Report {

	private static final long serialVersionUID = 1279347178772889570L;

	private AgileFascade fascade;
	private Integer rapidView;
	private List<Sprint> sprints;
	private Integer sprint;
	private Map<String, Double> developmentProgress;
	private Map<String, Double> testProgress;
	private String jsonData;
	private String testJsonData;
	private String testfloError;

	public Integer getRapidView() {
		return rapidView;
	}

	public void setRapidView(Integer rapidView) {
		this.rapidView = rapidView;
	}

	public AgileProgressReportAction(AgileFascade fascade) {
		this.fascade = fascade;
	}

	@Override
	public String doDefault() throws Exception {

		RestCallService restCallService = ComponentAccessor.getOSGiComponentInstanceOfType(RestCallService.class);
		if (!restCallService.testConnection()) {
			testfloError = "Cannot communicate with TestFLO - core plugin";
		}
		if (rapidView == null) {
			sprints = new ArrayList<Sprint>();
		} else {
			sprints = fascade.getSprints(rapidView);
		}

		if (sprints != null && sprints.size() > 0) {
			developmentProgress = fascade.getDevelopmentProgress(sprints.get(0).getId(), rapidView);
			testProgress = fascade.getTestProgress(sprints.get(0).getId(), rapidView);
		}
		Gson gson = new Gson();

		if (developmentProgress != null) {
			jsonData = gson.toJson(developmentProgress);
		}

		if (testProgress != null) {
			testJsonData = gson.toJson(testProgress);
		}

		return super.doDefault();
	}

	@Override
	protected String doExecute() throws Exception {
		doDefault();

		if (sprint != null) {
			developmentProgress = fascade.getDevelopmentProgress(sprint, rapidView);
			testProgress = fascade.getTestProgress(sprint, rapidView);
		}

		Gson gson = new Gson();

		if (developmentProgress != null) {
			jsonData = gson.toJson(developmentProgress);
		}

		if (testProgress != null) {
			testJsonData = gson.toJson(testProgress);
		}

		return super.doExecute();
	}

	public AgileFascade getFascade() {
		return fascade;
	}

	public void setFascade(AgileFascade fascade) {
		this.fascade = fascade;
	}

	public List<Sprint> getSprints() {
		return sprints;
	}

	public void setSprints(List<Sprint> sprints) {
		this.sprints = sprints;
	}

	public Integer getSprint() {
		return sprint;
	}

	public void setSprint(Integer sprint) {
		this.sprint = sprint;
	}

	public Map<String, Double> getDevelopmentProgress() {
		return developmentProgress;
	}

	public void setDevelopmentProgress(Map<String, Double> developmentProgress) {
		this.developmentProgress = developmentProgress;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public Map<String, Double> getTestProgress() {
		return testProgress;
	}

	public void setTestProgress(Map<String, Double> testProgress) {
		this.testProgress = testProgress;
	}

	public String getTestJsonData() {
		return testJsonData;
	}

	public void setTestJsonData(String testJsonData) {
		this.testJsonData = testJsonData;
	}

	@Override
	public String generateReportExcel(ProjectActionSupport arg0, Map arg1) throws Exception {
		return null;
	}

	@Override
	public String generateReportHtml(ProjectActionSupport arg0, Map arg1) throws Exception {
		return getRedirect("/secure/AgileProgressReport!default.jspa");
	}

	@Override
	public void init(ReportModuleDescriptor arg0) {
	}

	@Override
	public boolean isExcelViewSupported() {
		return false;
	}

	@Override
	public boolean showReport() {
		return true;
	}

	@Override
	public void validate(ProjectActionSupport arg0, Map arg1) {
	}

	public String getTestfloError() {
		return testfloError;
	}

	public void setTestfloError(String testfloError) {
		this.testfloError = testfloError;
	}

}
