package com.intenso.jira.plugins.tms.reports.reports;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
public class SubTasksColorStatusCFConfigBean {

	@JsonProperty
	private Long issueColorStatusCfId;

	@JsonProperty
	private String finishedStatusIds;

	@JsonProperty
	private String useDefaultStatusColor;

	@JsonProperty
	private String useSubtasks;

	@JsonProperty
	private String childIssueKeyCfId;

	@JsonProperty
	private String issueTypeIds;

	@JsonProperty
	private String statusColorMap;

	public SubTasksColorStatusCFConfigBean(Long issueColorStatusCfId, String finishedStatusIds, String useDefaultStatusColor, String useSubtasks, String childIssueKeyCfId, String issueTypeIds,
			String statusColorMap) {
		super();
		this.issueColorStatusCfId = issueColorStatusCfId;
		this.finishedStatusIds = finishedStatusIds;
		this.useDefaultStatusColor = useDefaultStatusColor;
		this.useSubtasks = useSubtasks;
		this.childIssueKeyCfId = childIssueKeyCfId;
		this.issueTypeIds = issueTypeIds;
		this.statusColorMap = statusColorMap;
	}

	public SubTasksColorStatusCFConfigBean() {
	}

	public Long getIssueColorStatusCfId() {
		return issueColorStatusCfId;
	}

	public void setIssueColorStatusCfId(Long issueColorStatusCfId) {
		this.issueColorStatusCfId = issueColorStatusCfId;
	}

	public String getFinishedStatusIds() {
		return finishedStatusIds;
	}

	public void setFinishedStatusIds(String finishedStatusIds) {
		this.finishedStatusIds = finishedStatusIds;
	}

	public String getUseDefaultStatusColor() {
		return useDefaultStatusColor;
	}

	public void setUseDefaultStatusColor(String useDefaultStatusColor) {
		this.useDefaultStatusColor = useDefaultStatusColor;
	}

	public String getUseSubtasks() {
		return useSubtasks;
	}

	public void setUseSubtasks(String useSubtasks) {
		this.useSubtasks = useSubtasks;
	}

	public String getChildIssueKeyCfId() {
		return childIssueKeyCfId;
	}

	public void setChildIssueKeyCfId(String childIssueKeyCfId) {
		this.childIssueKeyCfId = childIssueKeyCfId;
	}

	public String getIssueTypeIds() {
		return issueTypeIds;
	}

	public void setIssueTypeIds(String issueTypeIds) {
		this.issueTypeIds = issueTypeIds;
	}

	public String getStatusColorMap() {
		return statusColorMap;
	}

	public void setStatusColorMap(String statusColorMap) {
		this.statusColorMap = statusColorMap;
	}
}
