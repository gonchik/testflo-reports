package com.intenso.jira.plugins.tms.reports.integration.agile;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.user.ApplicationUser;
import com.google.gson.Gson;
import com.intenso.jira.plugins.tms.reports.integration.agile.filter.AgileLoginFilter;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.ContentWrapper;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.EstimateStatistic;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueView;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueViewWrapper;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.RapidView;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.RapidViewWrapper;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.Sprint;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.SprintWrapper;
import com.intenso.jira.plugins.tms.reports.integration.agile.visitor.EstimateIssueViewVisitor;
import com.intenso.jira.plugins.tms.reports.integration.agile.visitor.EstimateTestPlanVisitor;
import com.intenso.jira.plugins.tms.reports.integration.agile.visitor.IssueViewDecorator;
import com.intenso.jira.plugins.tms.reports.integration.agile.visitor.IssueViewStatus;

/**
 * @author InTENSO
 *
 */
public class AgileFascadeImpl implements AgileFascade {

	private static final Logger log = LoggerFactory.getLogger(AgileFascadeImpl.class);

	private static final String RAPID_VIEW_URL = "/rest/greenhopper/1.0/rapidview";
	private static final String SPRINT_IN_RAPID_VIEW_URL = "/rest/greenhopper/1.0/sprintquery/";
	private static final String SPRINT_URL = "/rest/greenhopper/1.0/rapid/charts/sprintreport";

	private static final String INCOMPLETED = "incompleted";
	private static final String COMPLETED = "completed";
	private static final String INCOMPLETED_PERCENTAGE = "incompletedPercentage";
	private static final String COMPLETED_PERCENTAGE = "completedPercentage";

	@Override
	public List<RapidView> getRapidViews() {

		List<RapidView> rapidView = new ArrayList<RapidView>();
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse response = null;
		try {
			httpClient = HttpClientBuilder.create().build();
			HttpGet get = new HttpGet(ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL) + RAPID_VIEW_URL);

			ApplicationUser au = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
			if (au != null) {
				get.addHeader(AgileLoginFilter.AGILE_SUITEST_HEADER, au.getKey());
			}

			response = httpClient.execute(get);

			Integer status = response.getStatusLine().getStatusCode();
			if (status >= 300) {
				return rapidView;
			}
			HttpEntity entity = response.getEntity();
			Gson gson = new Gson();

			String json = IOUtils.toString(entity.getContent());
			RapidViewWrapper rapidViewWrapper = gson.fromJson(json, RapidViewWrapper.class);
			rapidView = Arrays.asList(rapidViewWrapper.getViews());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}

			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}
		}
		return rapidView;
	}

	@Override
	public List<Sprint> getSprints(Integer rapidViewId) {
		List<Sprint> sprints = new ArrayList<Sprint>();
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse response = null;
		try {
			httpClient = HttpClientBuilder.create().build();
			HttpGet get = new HttpGet(ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL) + SPRINT_IN_RAPID_VIEW_URL + rapidViewId);

			ApplicationUser au = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
			if (au != null) {
				get.addHeader(AgileLoginFilter.AGILE_SUITEST_HEADER, au.getKey());
			}

			response = httpClient.execute(get);

			StatusLine statusLine = response.getStatusLine();
			Integer status = statusLine.getStatusCode();
			if (status >= 300) {
				return sprints;
			}
			HttpEntity entity = response.getEntity();
			Gson gson = new Gson();

			String json = IOUtils.toString(entity.getContent());
			SprintWrapper sprintWrapper = gson.fromJson(json, SprintWrapper.class);
			sprints = Arrays.asList(sprintWrapper.getSprints());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					log.error(e.getMessage());

				}
			}

			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}
		}
		return sprints;
	}

	@Override
	public IssueViewWrapper getIssues(Integer rapidViewId, Integer sprintId) {

		IssueViewWrapper issueViewWrapper = null;
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse response = null;
		try {
			httpClient = HttpClientBuilder.create().build();
			HttpGet get = new HttpGet(ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL) + SPRINT_URL + "?rapidViewId=" + rapidViewId + "&sprintId=" + sprintId);
			ApplicationUser au = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
			if (au != null) {
				get.addHeader(AgileLoginFilter.AGILE_SUITEST_HEADER, au.getKey());
			}

			response = httpClient.execute(get);

			Integer status = response.getStatusLine().getStatusCode();
			if (status >= 300) {
				return issueViewWrapper;
			}
			HttpEntity entity = response.getEntity();
			Gson gson = new Gson();

			StringWriter sw = new StringWriter();
			IOUtils.copy(entity.getContent(), sw);

			String json = sw.toString();
			issueViewWrapper = gson.fromJson(json, IssueViewWrapper.class);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}

			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}

		}
		return issueViewWrapper;
	}

	@Override
	public Map<String, Double> getDevelopmentProgress(Integer sprint, Integer rapidView) {

		Map<String, Double> result = new HashMap<String, Double>();

		IssueViewWrapper wrapper = getIssues(rapidView, sprint);
		if (wrapper == null)
			return result;
		ContentWrapper cw = wrapper.getContents();
		List<IssueView> completedIssues = Arrays.asList(cw.getCompletedIssues());
		List<IssueView> incompletedIssues = Arrays.asList(cw.getIncompletedIssues() == null ? new IssueView[] {} : cw.getIncompletedIssues());

		// calculate tracking estimate for completedIssues
		EstimateIssueViewVisitor completedVisitor = new EstimateIssueViewVisitor();
		double completedCalculation = 0d;
		if (completedIssues != null) {
			for (IssueView iv : completedIssues) {
				iv.accept(completedVisitor);
			}
		}
		completedCalculation = completedVisitor.getTotalEstimate();

		// calculate tracking estimate for incompletedIssues
		EstimateIssueViewVisitor incompletedVisitor = new EstimateIssueViewVisitor();
		double incompletedCalculation = 0d;
		if (incompletedIssues != null) {
			for (IssueView iv : incompletedIssues) {
				iv.accept(incompletedVisitor);
			}
		}
		incompletedCalculation = incompletedVisitor.getTotalEstimate();

		result.put(INCOMPLETED, incompletedCalculation);
		result.put(COMPLETED, completedCalculation);
		Double total = incompletedCalculation + completedCalculation;
		if (total > 0) {// otherwise division by 0
			result.put(INCOMPLETED_PERCENTAGE, Math.round(incompletedCalculation * 100.0 / total) / 1.0);
			result.put(COMPLETED_PERCENTAGE, Math.round(completedCalculation * 100.0 / total) / 1.0);
		}
		return result;
	}

	@Override
	public Map<String, Double> getTestProgress(Integer sprint, Integer rapidView) {

		Map<String, Double> testProgress = new HashMap<String, Double>();

		IssueViewWrapper wrapper = getIssues(rapidView, sprint);
		if (wrapper == null)
			return testProgress;
		ContentWrapper cw = wrapper.getContents();
		List<IssueView> completedIssues = Arrays.asList(cw.getCompletedIssues());
		List<IssueView> incompletedIssues = Arrays.asList(cw.getIncompletedIssues() == null ? new IssueView[] {} : cw.getIncompletedIssues());

		List<IssueViewDecorator> allIssues = new ArrayList<IssueViewDecorator>();

		for (IssueView iv : completedIssues) {
			allIssues.add(new IssueViewDecorator(iv, IssueViewStatus.COMPLETED));
		}

		for (IssueView iv : incompletedIssues) {
			allIssues.add(new IssueViewDecorator(iv, IssueViewStatus.INCOMPLETED));
		}

		EstimateTestPlanVisitor visitor = new EstimateTestPlanVisitor();

		for (IssueView iv : allIssues) {
			iv.accept(visitor);
		}

		List<String> satisfied = visitor.getSatisfiedRequirements();
		List<String> notSatisfied = visitor.getNotSatisfiedRequirements();

		Double incompleted = estimate(notSatisfied, allIssues);
		Double completed = estimate(satisfied, allIssues);
		Double total = incompleted + completed;

		testProgress.put(INCOMPLETED, incompleted);
		testProgress.put(COMPLETED, completed);

		if (total > 0) {
			testProgress.put(INCOMPLETED_PERCENTAGE, Math.round(incompleted * 100.0 / total) / 1.0);
			testProgress.put(COMPLETED_PERCENTAGE, Math.round(completed * 100.0 / total) / 1.0);
		}

		return testProgress;
	}

	private Double estimate(List<String> toEstimate, List<IssueViewDecorator> allIssues) {

		Double result = 0d;
		for (String key : toEstimate) {
			for (IssueViewDecorator iv : allIssues) {
				if (iv.getKey().equals(key)) {

					EstimateStatistic es = iv.getEstimateStatistic();
					if (es != null && es.getStatFieldValue() != null) {
						result += (es.getStatFieldValue().getValue() == null ? 0 : es.getStatFieldValue().getValue());
					}
					break;
				}

			}
		}

		return result;
	}

}
