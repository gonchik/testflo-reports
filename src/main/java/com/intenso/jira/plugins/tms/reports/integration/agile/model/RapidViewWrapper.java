package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RapidViewWrapper {

	@JsonProperty
	private RapidView[] views;

	public RapidView[] getViews() {
		return views;
	}

	public void setViews(RapidView[] views) {
		this.views = views;
	}

}
