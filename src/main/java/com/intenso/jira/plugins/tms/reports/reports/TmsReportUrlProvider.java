package com.intenso.jira.plugins.tms.reports.reports;

import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.plugin.report.ReportUrlProvider;

/**
 * @author InTENSO
 *
 */
public class TmsReportUrlProvider implements ReportUrlProvider {

	@Override
	public Option<String> getUrl(ReportModuleDescriptor descriptor, Map<String, Object> context) {
		Long rapidView = (Long) context.get("selectedBoardId");
		return Option.option("/secure/AgileProgressReport!default.jspa?rapidView=" + rapidView.toString());
	}

}