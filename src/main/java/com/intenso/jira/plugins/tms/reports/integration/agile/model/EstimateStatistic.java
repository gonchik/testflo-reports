package com.intenso.jira.plugins.tms.reports.integration.agile.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EstimateStatistic {

	@JsonProperty
	private String statFieldId;
	@JsonProperty
	private TrackingValue statFieldValue;

	public String getStatFieldId() {
		return statFieldId;
	}

	public void setStatFieldId(String statFieldId) {
		this.statFieldId = statFieldId;
	}

	public TrackingValue getStatFieldValue() {
		return statFieldValue;
	}

	public void setStatFieldValue(TrackingValue statFieldValue) {
		this.statFieldValue = statFieldValue;
	}

}
