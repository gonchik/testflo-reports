package com.intenso.jira.plugins.tms.export.rest;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author InTENSO
 *
 */
public class TmsConfigurationBean {

	@JsonProperty
	private Long exportFilterId;
	
	@JsonProperty
	private Long exportTcFilterId;
	
	@JsonProperty
	private Long exportTpFilterId;
	
	@JsonProperty
	private String testCaseIssueTypeId;
	
	@JsonProperty
	private String testPlanIssueTypeId;
	
	@JsonProperty
	private String testCaseClosedStatusIds;

	@JsonProperty
	private String testCaseAssignRoles;

	@JsonProperty
	private Long maxResult;

	@JsonProperty
	private Long testCaseColumnCfg;

	@JsonProperty
	private String testCaseAssignalbeRoles;

	@JsonProperty
	private String testCaseDeleteableStatusesIds;

	@JsonProperty
	private Long testCaseUpdateFieldsFilterId;

	@JsonProperty
	private Long testCaseCreateFieldsFilterId;

	@JsonProperty
	private Long defectsLinkId;

	@JsonProperty
	private String testCaseTemplateActiveStatusIds;

	@JsonProperty
	private String testCaseTemplateIssueTypeId;

	@JsonProperty
	private TmsCustomFieldsBean customFields;
	
	
	
	public Long getExportFilterId() {
		return exportFilterId;
	}
	public void setExportFilterId(Long exportFilterId) {
		this.exportFilterId = exportFilterId;
	}
	public Long getExportTcFilterId() {
		return exportTcFilterId;
	}
	public void setExportTcFilterId(Long exportTcFilterId) {
		this.exportTcFilterId = exportTcFilterId;
	}
	public Long getExportTpFilterId() {
		return exportTpFilterId;
	}
	public void setExportTpFilterId(Long exportTpFilterId) {
		this.exportTpFilterId = exportTpFilterId;
	}
	public String getTestCaseIssueTypeId() {
		return testCaseIssueTypeId;
	}
	public void setTestCaseIssueTypeId(String testCaseIssueTypeId) {
		this.testCaseIssueTypeId = testCaseIssueTypeId;
	}
	public String getTestPlanIssueTypeId() {
		return testPlanIssueTypeId;
	}
	public void setTestPlanIssueTypeId(String testPlanIssueTypeId) {
		this.testPlanIssueTypeId = testPlanIssueTypeId;
	}
	public String getTestCaseClosedStatusIds() {
		return testCaseClosedStatusIds;
	}
	public void setTestCaseClosedStatusIds(String testCaseClosedStatusIds) {
		this.testCaseClosedStatusIds = testCaseClosedStatusIds;
	}
	public void setMaxTctSelectorIssueCount(Long maxResult) {
		this.maxResult = maxResult;
	}
	public void setTestCaseColumnCfg(Long testCaseFilterId) {
		this.testCaseColumnCfg = testCaseFilterId;
	}
	public void setTestCaseAssignRoles(String join) {
		this.testCaseAssignRoles = join;
	}
	public void setTestCaseAssignalbeRoles(String join) {
		this.testCaseAssignalbeRoles = join;
	}
	public void setTestCaseDeleteableStatusesIds(String join) {
		this.testCaseDeleteableStatusesIds = join;
	}
	public void setTestCaseUpdateFieldsFilterId(Long fieldsToUpdateFilter) {
		this.testCaseUpdateFieldsFilterId = fieldsToUpdateFilter;
	}
	public void setTestCaseCreateFieldsFilterId(Long fieldsToCreateFilter) {
		this.testCaseCreateFieldsFilterId = fieldsToCreateFilter;
	}
	public void setDefectsLinkId(Long testCaseDefectLinkId) {
		this.defectsLinkId = testCaseDefectLinkId;
	}
	public void setTestCaseTemplateActiveStatusIds(String string) {
		this.testCaseTemplateActiveStatusIds = string;
	}
	public void setTestCaseTemplateIssueTypeId(String testCaseTemplateIssueTypeId) {
		this.testCaseTemplateIssueTypeId = testCaseTemplateIssueTypeId;
	}
	public void setCustomFields(TmsCustomFieldsBean cfBean) {
		this.customFields = cfBean;
	}
	public String getTestCaseAssignRoles() {
		return testCaseAssignRoles;
	}
	public Long getMaxResult() {
		return maxResult;
	}
	public Long getTestCaseColumnCfg() {
		return testCaseColumnCfg;
	}
	public String getTestCaseAssignalbeRoles() {
		return testCaseAssignalbeRoles;
	}
	public String getTestCaseDeleteableStatusesIds() {
		return testCaseDeleteableStatusesIds;
	}
	public Long getTestCaseUpdateFieldsFilterId() {
		return testCaseUpdateFieldsFilterId;
	}
	public Long getTestCaseCreateFieldsFilterId() {
		return testCaseCreateFieldsFilterId;
	}
	public Long getDefectsLinkId() {
		return defectsLinkId;
	}
	public String getTestCaseTemplateActiveStatusIds() {
		return testCaseTemplateActiveStatusIds;
	}
	public String getTestCaseTemplateIssueTypeId() {
		return testCaseTemplateIssueTypeId;
	}
	public TmsCustomFieldsBean getCustomFields() {
		return customFields;
	}
	
}
