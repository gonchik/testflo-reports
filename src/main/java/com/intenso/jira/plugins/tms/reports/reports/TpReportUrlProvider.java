package com.intenso.jira.plugins.tms.reports.reports;

import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.plugin.report.ReportUrlProvider;

/**
 * @author InTENSO
 *
 */
public class TpReportUrlProvider implements ReportUrlProvider {

	@Override
	public Option<String> getUrl(ReportModuleDescriptor descriptor, Map<String, Object> context) {
		return Option.option("/secure/TpProgressReport!default.jspa");
	}

}
