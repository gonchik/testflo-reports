package com.intenso.jira.plugins.tms.reports.integration.agile.visitor;

import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueView;

/**
 * @author InTENSO
 *
 */
public interface Visitor {

	public void visitElement(IssueView issueView);
	
}
