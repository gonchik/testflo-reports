package com.intenso.jira.plugins.tms.export.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Entities.EscapeMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.intenso.jira.plugins.tms.export.service.ExportService;

/**
 * @author InTENSO
 *
 */
public class TmsExportToPdfServlet extends HttpServlet {
	private static final long serialVersionUID = -8347654225798536041L;

	Logger log = LoggerFactory.getLogger(TmsExportToPdfServlet.class);

	private final ExportService exportService;

	public TmsExportToPdfServlet(ExportService exportService) {
		this.exportService = exportService;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		OutputStream outputStream = null;
		try {
			response.setContentType("application/pdf");
			outputStream = response.getOutputStream();
			String data = exportService.generateHtml(request.getParameter("issueKey"), request.getParameter("urlQuery"), request.getParameter("sourceIssue"));

			org.jsoup.nodes.Document doc = Jsoup.parseBodyFragment(data);
			doc.outputSettings().escapeMode(EscapeMode.xhtml).charset(StandardCharsets.UTF_8.name());

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(org.apache.stanbol.enhancer.engines.htmlextractor.impl.DOMBuilder.jsoup2DOM(doc), null);
			renderer.layout();
			renderer.createPDF(outputStream);
			outputStream.close();

		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
		}
	}
}