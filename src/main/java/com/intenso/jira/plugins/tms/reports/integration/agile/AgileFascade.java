package com.intenso.jira.plugins.tms.reports.integration.agile;

import java.util.List;
import java.util.Map;

import com.intenso.jira.plugins.tms.reports.integration.agile.model.IssueViewWrapper;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.RapidView;
import com.intenso.jira.plugins.tms.reports.integration.agile.model.Sprint;

/**
 * @author InTENSO
 *
 */
public interface AgileFascade {

	List<RapidView> getRapidViews();
	List<Sprint> getSprints(Integer rapidViewId);
	IssueViewWrapper getIssues(Integer rapidViewId, Integer sprintId);
	Map<String, Double> getDevelopmentProgress(Integer sprint, Integer rapidView);
	Map<String, Double> getTestProgress(Integer sprint, Integer rapidView);

}
