package com.intenso.jira.plugins.tms.reports.reports;

import java.util.HashSet;
import java.util.List;

import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.ChangedClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.ClauseVisitor;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.WasClause;

/**
 * @author InTENSO
 *
 */
public class QueryVisitor implements ClauseVisitor<Clause> {

	private HashSet<String> terminalSet = new HashSet<String>();
	private Boolean projectIsSet = Boolean.FALSE;
	private Integer projectClauseCount = 0;

	@Override
	public Clause visit(AndClause and) {
		List<Clause> clauses = and.getClauses();
		if (clauses != null) {
			for (Clause c : clauses) {
				c.accept(this);
			}
		}

		return null;
	}

	@Override
	public Clause visit(NotClause not) {
		List<Clause> clauses = not.getClauses();
		if (clauses != null) {
			for (Clause c : clauses) {
				c.accept(this);
			}
		}
		return null;
	}

	@Override
	public Clause visit(OrClause or) {

		List<Clause> clauses = or.getClauses();
		if (clauses != null) {
			for (Clause c : clauses) {
				c.accept(this);
			}
		}
		return null;
	}

	@Override
	public Clause visit(TerminalClause terminal) {
		if (terminal.getName().equalsIgnoreCase("issuetype")) {
			terminalSet.add(terminal.getOperand().getDisplayString().replaceAll("\"", ""));
		}

		if (terminal.getName().equalsIgnoreCase("project")) {
			projectIsSet = Boolean.TRUE;
			projectClauseCount++;
		}

		return null;
	}

	@Override
	public Clause visit(WasClause was) {
		List<Clause> clauses = was.getClauses();
		if (clauses != null) {
			for (Clause c : clauses) {
				c.accept(this);
			}
		}
		return null;
	}

	@Override
	public Clause visit(ChangedClause changed) {
		List<Clause> clauses = changed.getClauses();
		if (clauses != null) {
			for (Clause c : clauses) {
				c.accept(this);
			}
		}
		return null;
	}

	public HashSet<String> getIssueType() {
		return terminalSet;
	}

	public Boolean getProjectIsSet() {
		return projectIsSet;
	}

	public void setProjectIsSet(Boolean projectIsSet) {
		this.projectIsSet = projectIsSet;
	}

	public Integer getProjectClauseCount() {
		return projectClauseCount;
	}

	public void setProjectClauseCount(Integer projectClauseCount) {
		this.projectClauseCount = projectClauseCount;
	}

}
