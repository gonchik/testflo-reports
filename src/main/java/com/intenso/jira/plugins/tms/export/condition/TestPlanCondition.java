package com.intenso.jira.plugins.tms.export.condition;

import java.util.List;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.intenso.jira.plugins.tms.export.rest.RestCallService;
import com.intenso.jira.plugins.tms.export.rest.TmsConfigurationBean;

/**
 * @author InTENSO
 *
 */
public class TestPlanCondition extends AbstractWebCondition {

	private final RestCallService restCallService;

	public TestPlanCondition(RestCallService restCallService) {
		this.restCallService = restCallService;
	}

	@Override
	public boolean shouldDisplay(ApplicationUser arg0, JiraHelper jiraHelper) {
		Issue issue = (Issue) jiraHelper.getContextParams().get("issue");
		TmsConfigurationBean configuration = restCallService.getConfiguration();
		if (configuration == null) {
			return false;
		}
		String testPlanIssueTypeId = configuration.getTestPlanIssueTypeId();

		boolean isTestPlan = issue.getIssueTypeId().equals(testPlanIssueTypeId);
		if (!isTestPlan) {
			List<String> tpIds = restCallService.getAdditionalTestPlanIssueTypes(issue.getProjectObject().getId());
			if (tpIds == null) {
				return false;
			}
			for (String id : tpIds) {
				if (issue.getIssueTypeId().equals(id)) {
					isTestPlan = true;
					break;
				}
			}
		}

		return isTestPlan;
	}

}
